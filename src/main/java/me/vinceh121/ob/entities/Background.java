package me.vinceh121.ob.entities;

public class Background {
	private int id;
	private String name, url, deskUrl;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDeskUrl() {
		return deskUrl;
	}

	public void setDeskUrl(String deskUrl) {
		this.deskUrl = deskUrl;
	}
}
