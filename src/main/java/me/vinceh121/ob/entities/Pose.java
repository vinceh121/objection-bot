package me.vinceh121.ob.entities;

import java.util.List;

public class Pose {
	private int id;
	private String name, idleImageUrl, speakImageUrl, musicFileName;
	private List<State> states;
	private List<AudioTick> audioTicks;
	private List<FunctionTick> functionTicks;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdleImageUrl() {
		return idleImageUrl;
	}

	public void setIdleImageUrl(String idleImageUrl) {
		this.idleImageUrl = idleImageUrl;
	}

	public String getSpeakImageUrl() {
		return speakImageUrl;
	}

	public void setSpeakImageUrl(String speakImageUrl) {
		this.speakImageUrl = speakImageUrl;
	}

	public String getMusicFileName() {
		return musicFileName;
	}

	public void setMusicFileName(String musicFileName) {
		this.musicFileName = musicFileName;
	}

	public List<State> getStates() {
		return states;
	}

	public void setStates(List<State> states) {
		this.states = states;
	}

	public List<AudioTick> getAudioTicks() {
		return audioTicks;
	}

	public void setAudioTicks(List<AudioTick> audioTicks) {
		this.audioTicks = audioTicks;
	}

	public List<FunctionTick> getFunctionTicks() {
		return functionTicks;
	}

	public void setFunctionTicks(List<FunctionTick> functionTicks) {
		this.functionTicks = functionTicks;
	}

	public static class State {
		private String imageUrl;
		private int id, nextPoseDelay, speakDelay;
		private boolean playAtTextEnd;

		public String getImageUrl() {
			return imageUrl;
		}

		public void setImageUrl(String imageUrl) {
			this.imageUrl = imageUrl;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public int getNextPoseDelay() {
			return nextPoseDelay;
		}

		public void setNextPoseDelay(int nextPoseDelay) {
			this.nextPoseDelay = nextPoseDelay;
		}

		public int getSpeakDelay() {
			return speakDelay;
		}

		public void setSpeakDelay(int speakDelay) {
			this.speakDelay = speakDelay;
		}

		public boolean isPlayAtTextEnd() {
			return playAtTextEnd;
		}

		public void setPlayAtTextEnd(boolean playAtTextEnd) {
			this.playAtTextEnd = playAtTextEnd;
		}
	}

	public static class AudioTick {
		private String fileName;
		private int time, volume;
		private boolean playAtTextEnd;

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public int getTime() {
			return time;
		}

		public void setTime(int time) {
			this.time = time;
		}

		public int getVolume() {
			return volume;
		}

		public void setVolume(int volume) {
			this.volume = volume;
		}

		public boolean isPlayAtTextEnd() {
			return playAtTextEnd;
		}

		public void setPlayAtTextEnd(boolean playAtTextEnd) {
			this.playAtTextEnd = playAtTextEnd;
		}
	}

	public static class FunctionTick {
		private String functionName, functionParam;
		private int time;

		public String getFunctionName() {
			return functionName;
		}

		public void setFunctionName(String functionName) {
			this.functionName = functionName;
		}

		public String getFunctionParam() {
			return functionParam;
		}

		public void setFunctionParam(String functionParam) {
			this.functionParam = functionParam;
		}

		public int getTime() {
			return time;
		}

		public void setTime(int time) {
			this.time = time;
		}
	}
}
