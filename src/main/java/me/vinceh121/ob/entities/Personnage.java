package me.vinceh121.ob.entities;

import java.util.List;

/**
 * This should be named "Character" but I'm too lazy to resolve conflicts with
 * java.lang.Character so French it is
 */
public class Personnage {
	private int id;
	private String name, namePlate, side, series, bubbleTypes;
	private double objectionVolume;
	private Background background;
	private List<Pose> poses;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNamePlate() {
		return namePlate;
	}

	public void setNamePlate(String namePlate) {
		this.namePlate = namePlate;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getBubbleTypes() {
		return bubbleTypes;
	}

	public void setBubbleTypes(String bubbleTypes) {
		this.bubbleTypes = bubbleTypes;
	}

	public double getObjectionVolume() {
		return objectionVolume;
	}

	public void setObjectionVolume(double objectionVolume) {
		this.objectionVolume = objectionVolume;
	}

	public Background getBackground() {
		return background;
	}

	public void setBackground(Background background) {
		this.background = background;
	}

	public List<Pose> getPoses() {
		return poses;
	}

	public void setPoses(List<Pose> poses) {
		this.poses = poses;
	}
}
