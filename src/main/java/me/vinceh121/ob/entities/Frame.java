package me.vinceh121.ob.entities;

import java.util.Collections;
import java.util.List;

public class Frame {
	private int id = -1, poseId, bubbleType;
	private Integer popupId = null, characterId = null; // fuck json
	private String text, username = "";
	private boolean mergeNext, doNotTalk, goNext, poseAnimation = true, flipped;
	private List<Action> frameActions = Collections.emptyList();
	private List<Fade> frameFades = Collections.emptyList();
	private Background background;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPoseId() {
		return poseId;
	}

	public void setPoseId(int poseId) {
		this.poseId = poseId;
	}

	public int getBubbleType() {
		return bubbleType;
	}

	public void setBubbleType(int bubbleType) {
		this.bubbleType = bubbleType;
	}

	public Integer getCharacterId() {
		return characterId;
	}

	public void setCharacterId(Integer characterId) {
		this.characterId = characterId;
	}

	public Integer getPopupId() {
		return popupId;
	}

	public void setPopupId(Integer popupId) {
		this.popupId = popupId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isMergeNext() {
		return mergeNext;
	}

	public void setMergeNext(boolean mergeNext) {
		this.mergeNext = mergeNext;
	}

	public boolean isDoNotTalk() {
		return doNotTalk;
	}

	public void setDoNotTalk(boolean doNotTalk) {
		this.doNotTalk = doNotTalk;
	}

	public boolean isGoNext() {
		return goNext;
	}

	public void setGoNext(boolean goNext) {
		this.goNext = goNext;
	}

	public boolean isPoseAnimation() {
		return poseAnimation;
	}

	public void setPoseAnimation(boolean poseAnimation) {
		this.poseAnimation = poseAnimation;
	}

	public boolean isFlipped() {
		return flipped;
	}

	public void setFlipped(boolean flipped) {
		this.flipped = flipped;
	}

	public List<Action> getFrameActions() {
		return frameActions;
	}

	public void setFrameActions(List<Action> frameActions) {
		this.frameActions = frameActions;
	}

	public List<Fade> getFrameFades() {
		return frameFades;
	}

	public void setFrameFades(List<Fade> frameFades) {
		this.frameFades = frameFades;
	}

	public Background getBackground() {
		return background;
	}

	public void setBackground(Background background) {
		this.background = background;
	}
}
