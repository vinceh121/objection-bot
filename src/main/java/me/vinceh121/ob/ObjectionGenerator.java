package me.vinceh121.ob;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import me.vinceh121.ob.entities.Frame;
import me.vinceh121.ob.entities.Personnage;
import me.vinceh121.ob.entities.Pose;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;

public class ObjectionGenerator {
	private static final Random RNG = new Random();
	private final List<Personnage> characters;
	/**
	 * Key: Discord ID
	 * Value: Objection.Lol character id
	 */
	private Map<String, Integer> memberCharMap;
	private List<Message> messages;
	private List<Frame> frames;

	public ObjectionGenerator(final List<Personnage> characters) {
		this.characters = characters;
	}

	public List<Message> getMessages() {
		return this.messages;
	}

	public void setMessages(final List<Message> messages) {
		this.messages = messages;
	}

	private int getUnusedCharacterId() {
		int id;
		while (this.memberCharMap.containsValue(id = RNG.nextInt(this.characters.size()))) {}
		return id;
	}

	private void mapMemberCharacters() {
		this.memberCharMap = new HashMap<>();
		for (final Message msg : this.messages) {
			final User mem = msg.getAuthor();
			if (!this.memberCharMap.containsKey(mem.getId())) {
				this.memberCharMap.put(mem.getId(), this.getUnusedCharacterId());
			}
		}
	}

	private int getRandomPoseForCharacter(final int ch) {
		final List<Pose> poses = this.characters.get(ch).getPoses();
		return poses.get(RNG.nextInt(poses.size())).getId();
	}

	private Frame makeFrame(final Message msg) {
		final Frame frame = new Frame();
		// frame.setCharacterId(this.memberCharMap.get(msg.getAuthor().getId())); // DO
		// NOT SET
		frame.setUsername(msg.getAuthor().getName());
		frame.setText(msg.getContentStripped());
		frame.setPoseId(this.getRandomPoseForCharacter(this.memberCharMap.get(msg.getAuthor().getId())));
		return frame;
	}

	public void generateFrames() {
		this.frames = new ArrayList<>();
		this.mapMemberCharacters();
		for (int i = this.messages.size() - 1; i >= 0; i--) {
			this.frames.add(this.makeFrame(this.messages.get(i)));
		}
	}

	public List<Frame> getFrames() {
		return this.frames;
	}

	public Map<String, Integer> getMemberCharMap() {
		return memberCharMap;
	}
}
