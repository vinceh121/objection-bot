package me.vinceh121.ob;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Date;
import java.util.List;

import javax.security.auth.login.LoginException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import me.vinceh121.ob.entities.Personnage;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.MessageHistory.MessageRetrieveAction;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public class ObjectionBot extends ListenerAdapter {
	public static final String[] PREFIXES = { "&" };
	private static final Logger LOG = LogManager.getLogger(ObjectionBot.class);
	private static final ObjectMapper MAPPER = new ObjectMapper();
	private final Config config;
	private final JDA jda;
	private final List<Personnage> characters;

	public static void main(final String[] args) {
		try {
			new ObjectionBot();
		} catch (LoginException e) {
			e.printStackTrace();
		}
	}

	public ObjectionBot() throws LoginException {
		try {
			this.config = MAPPER.readValue(new File("/etc/objection-bot/config.json"), Config.class);
			this.characters = MAPPER.readValue(new File("/etc/objection-bot/characters.json"),
					new TypeReference<List<Personnage>>() {});
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}

		final JDABuilder jdaBuilder = JDABuilder.createDefault(this.config.getToken());
		this.jda = jdaBuilder.build();
		this.jda.addEventListener(this);
	}

	private static void sendHelp(final TextChannel chl) {
		chl.sendMessage("@Objection bot#0098 help\n" + "@Objection bot#0098\n" + "@Objection bot#0098 <message id>")
				.queue();
	}

	@Override
	public void onGuildMessageReceived(final GuildMessageReceivedEvent event) {
		if (!event.getMessage().isMentioned(this.jda.getSelfUser())) {
			return;
		}

		final String[] args = event.getMessage().getContentRaw().split(" ");

		if (args.length > 2) {
			sendHelp(event.getChannel());
			return;
		}

		final MessageRetrieveAction action;

		if (args.length == 1) {
			action = event.getChannel().getHistoryBefore(event.getMessage(), 30);
		} else {
			try {
				final long id = Long.parseLong(args[1]);
				action = event.getChannel().getHistoryAround(id, 30);
			} catch (final NumberFormatException e) {
				sendHelp(event.getChannel());
				return;
			}
		}

		action.queue(this::sendTheStuff, t -> {
			LOG.error("Error while fetching history", t);
			event.getChannel().sendMessage("oopsie fucky we faiwed to get ffe messages").queue();
		});
	}

	private void sendTheStuff(final MessageHistory hist) {
		final ObjectionGenerator gen = new ObjectionGenerator(this.characters);
		gen.setMessages(hist.getRetrievedHistory());
		gen.generateFrames();
		try {
			final byte[] b64Json = Base64.getEncoder().encode(MAPPER.writeValueAsBytes(gen.getFrames()));
			hist.getChannel()
					.sendFile(b64Json, "Objection-" + hist.getChannel().getId() + "-" + new Date() + ".objection")
					.queue(null, t -> LOG.error("Error while sending message", t));
		} catch (final JsonProcessingException e) {
			LOG.error("Error in processing JSON", e);
		}
	}
}
